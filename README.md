# mokit-inputlibrary

This is MOKIT's input library which collects input files and instructions for running MOKIT. A few typical kinds of cases will be included in this repository:
* Examples for automr which show that the automatic MR workflow works (but not the most simple ones which stay in MOKIT examples). Also, we can include some cases that do not work perfectly so we can try to improve the strategy.
* Examples that cannot be dealt with current automr strategy so we need to do some manual/semi-manual step.

