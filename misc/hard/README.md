# Hard cases

## ring opening of dioxirane
The reactant, TS and product (denoted as diox0, 1, 2) are simple CAS(2,2) case when separately treated. But if a shared active space is desired, it turns into CAS(6e,4o), with two near-doubly-occupied orbitals in AS, which suffers from undesired active-occupied rotation.

If using MOKIT, we have to do CASSCF(2,2) (or auto CASSCF, it doesn't matter) first, and then CASSCF(6e,4o) with ist=5. For diox1 it works but diox0,diox2 fail.

Another approach is to use symmCASSCF of pyscf. With
```
mc = mcscf.CASSCF(mf, 4, 6)
cassym = {'A1':1,'A2':1,'B1':1,'B2':1}
mo = mcscf.sort_mo_by_irrep(mc, mf.mo_coeff, cassym)
mc.kernel(mo)
```
it works for diox0. But diox1 fails.

For diox1 I managed to converge it with
```
mc = mcscf.CASSCF(mf, 4, 6)
cassym = {'A1':1,'A2':1,'B1':1,'B2':1}
mo = mcscf.sort_mo_by_irrep(mc, mf.mo_coeff, cassym)
#mc.ah_grad_trust_region = 1.0
mc.frozen = 6
mc.kernel(mo)
mc.analyze()

mc.frozen = 0
mc.max_stepsize = 0.01
mc.max_cycle_micro = 2
mc.kernel()
mc.analyze()
```
