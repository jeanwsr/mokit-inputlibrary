# future features

## nonstd/extralinks

A long time goal could be supporting workflows of different methods in arbitrary order,
 instead of the order determined by program, but that's hard to implement.
At least, we can first consider a few simple non-standard workflows like
* CASSCF -> {arbitrary order of post-CASSCF steps} (safe and easy, because those post steps don't affect each other)
* HF -> {arbitrary order of post-HF steps} (similarly)
* multiple CASSCF steps in a row (like different active spaces, different states)
* {arbitrary combination of UNO, localize, asrot, find_antibonding(and possibly add options for each step)} -> CASSCF (not safe enough)

A proposal for input syntax can be, nonstd style:
```
%nproc=4
%mem=4gb
# nonstd/def2svp 

mokit{}
CASSCF ! indicating the steps before is automatically determined
NEVPT2
CASPT2-K

0 1
[geom]
```
or extralinks style
```
%nproc=4
%mem=4gb
# CASSCF/def2svp extralinks=(NEVPT2,CASPT2-K)

mokit{}
```

The Gaussian's nonstd actually put the paths below route line but it seems putting them below title line looks better. 
And that's still valid gjf (just with multiple lines of title) for GaussView and Multiwfn. 

Moreover we could add options for each step in nonstd style
```
mokit{GlobalOption=xxx}
CASSCF{}
NEVPT2{Option=xxx}
CASPT2-K{Option2=xxx}
```
But that's not so necessary and not easy to implement.

And also multiple CASSCF steps
```
mokit{GlobalOption=xxx}
CASSCF
CASSCF{Nstates=2}
```

## pop

Some lightweight population or composition analysis should be enabled in each mokit job (not enabled currently for pyscf?).

## symm
* on-the-fly symmetry in PySCF

## test
For python part, using `pytest` is straightforward. For Fortran part, consider `fortuno`?


