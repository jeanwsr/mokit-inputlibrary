# input library for automr

## normal 
* CH3OO, an example for generation and visualization of bonding and anti-bonding orbitals. See also [keinbbs](http://bbs.keinsci.com/forum.php?mod=redirect&goto=findpost&ptid=322&pid=201966).

## hard
* mb_im09, a conformation of m-benzyne. With weak spin contamination in UHF, UNO-GVB fails to collect enough pairs (9 vs 14), leading to the final CASSCF(2,2) result (right solution is (8,8)). Using `ist=3` solves the problem.

TODO:

Normal automatic calc
* CASSCF of [Re2Cl8]2-, [keinbbs](http://bbs.keinsci.com/forum.php?mod=redirect&goto=findpost&ptid=45144&pid=291417)
* CASSCF of ethanol triplet, [keinbbs](http://bbs.keinsci.com/thread-3529-1-1.html)
* CASSCF of B3, [keinbbs](http://bbs.keinsci.com/thread-17905-1-1.html)
* GVB B2H6

More custom ones
* find anti-bonding orbitals of SOMOs. e.g. V2 or benzene cation.
* O2 T0 and S1, [keinbbs](http://bbs.keinsci.com/thread-37537-1-1.html)
* active space for S0 and T1, [keinbbs](http://bbs.keinsci.com/thread-19538-1-1.html)
* faster GVB step for large number of AOs.

Benchmarking strategies
* conjugated pi systems
  + polyacenes (3-7 rings)
  + [n]cycloacenes, 2,2-bipyridine anion (https://doi.org/10.1021/acs.jctc.3c00949)
